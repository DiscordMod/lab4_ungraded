public class Student
{
	private double studyTime;
	
	private double sleepTime;
	
	private String education;
	
	private int amountLearnt;
	
	//constructor
	
	public Student(double studyTime, String education)
	{
		this.studyTime = studyTime;
		this.sleepTime = sleepTime;
		this.education = education;
		this.amountLearnt = 0;
	}
	
	
	public void setStudyTime(double studyTime)
	{
		if(studyTime > 0 && studyTime < 24){
		this.studyTime = studyTime;
		}
	}
	public void setSleepTime(double sleepTime) 
	{
		if(sleepTime > 0 && sleepTime < 24){
		this.sleepTime = sleepTime;
		}
	}
	public void setEducation(String education)
	{
		this.education = education;
	}
	public void setAmountLearnt(int amountLearnt)
	{
		if(amountLearnt > 0){
		this.amountLearnt = amountLearnt;
		}
	}
	
	
	public double getStudyTime()
	{
		return this.studyTime;
	}
	public double getSleepTime() 
	{
       return this.sleepTime;
	}
	public String getEducation()
	{
		return this.education;
	}
	public int getAmountLearnt()
	{
		return this.amountLearnt;
	}
	
	public void educationIn(String education)
	{
		System.out.println("I currently study at " + education);
		
	}
	public double sleep(double sleep)
	{
		//student sleeps a bit more due to difficulty in waking up
		return sleep+0.1;
		
	}
	public void learn(int amountStudied)
	{
		amountLearnt+=amountStudied;
	}
}
